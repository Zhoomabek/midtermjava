package kz.mid.aitu.mid.advancedJavaMid.repository;

import kz.mid.aitu.mid.advancedJavaMid.model.Person;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PersonRepository extends CrudRepository<Person, Long> {}