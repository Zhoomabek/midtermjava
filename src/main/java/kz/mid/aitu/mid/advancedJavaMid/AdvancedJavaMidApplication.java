package kz.mid.aitu.mid.advancedJavaMid;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AdvancedJavaMidApplication {

	public static void main(String[] args) {
		SpringApplication.run(AdvancedJavaMidApplication.class, args);
	}}
