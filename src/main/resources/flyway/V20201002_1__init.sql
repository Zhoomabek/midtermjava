CREATE TABLE person(
    id serial,
    firstname VARCHAR (32),
    lastname VARCHAR (32),
    city VARCHAR (16),
    phone VARCHAR (16),
    telegram VARCHAR (16)
);

ALTER TABLE person owner to postgres;